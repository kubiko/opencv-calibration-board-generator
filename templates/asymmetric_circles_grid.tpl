<svg
  width="${svg_width}${svg_unit}" height="${svg_height}${svg_unit}"
  viewBox="0 0 ${svg_width} ${svg_height}"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:py="http://genshi.edgewall.org/">
  <g>
    <title>${calibration_pattern}</title>
    <rect x="${margin_x}" y="${margin_y}" width="${2*width*square_size}" height="${height*square_size}" fill="#FFFFFF" />
    <g py:for="y in range(height)">
    <py:for each="x in range(width)">
      <circle cx="${margin_x + (0.5 + (2 * x + y % 2)) * square_size}" cy="${margin_y + (y + 0.5) * square_size}" r="${0.4 * square_size}" fill="#000000" />
    </py:for>
    </g>
  </g>
</svg>

