<svg
  width="${svg_width}${svg_unit}" height="${svg_height}${svg_unit}"
  viewBox="0 0 ${svg_width} ${svg_height}"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:py="http://genshi.edgewall.org/">
  <g>
    <title>${calibration_pattern}</title>
    <rect x="${margin_x}" y="${margin_y}" width="${(width+1)*square_size}" height="${(height+1)*square_size}" fill="#FFFFFF" />
    <g py:for="y in range(height+1)">
    <py:for each="x in range(width+1)">
    <py:choose test="((y % 2) + (x % 2)) % 2">
      <rect py:when="0" x="${margin_x + x * square_size}" y="${margin_y + y * square_size}" width="${square_size}" height="${square_size}" fill="#000000" />
    </py:choose>
    </py:for>
    </g>
  </g>
</svg>

