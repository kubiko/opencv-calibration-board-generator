#!/usr/bin/env python

import argparse

from genshi.template import MarkupTemplate

parser = argparse.ArgumentParser(description='Create callibration board SVG file')
parser.add_argument('calibration_pattern', choices=['chessboard', 'circles_grid', 'asymmetric_circles_grid'], default='chessboard')
parser.add_argument('-x', '--width', default='9', type=int)
parser.add_argument('-y', '--height', default='6', type=int)
parser.add_argument('-s', '--square-size', default='50', type=int)
parser.add_argument('-m', '--margin', default=0, type=int)
parser.add_argument('--svg-width', default=0, type=int)
parser.add_argument('--svg-height', default=0, type=int)
parser.add_argument('--svg-unit', choices=['em', 'ex', 'px', 'pt', 'pc', 'cm', 'mm', 'in'], default='mm', type=str)
parser.add_argument('--svg-template', default='templates/%(calibration_pattern)s.tpl', type=str)
parser.add_argument('--svg-output', default='%(calibration_pattern)s.svg', type=str)
parser.add_argument('--xml-template', default='templates/opencv_config.tpl', type=str)
parser.add_argument('--xml-output', default='%(calibration_pattern)s.xml', type=str)

args = parser.parse_args()

data = vars(args)

data['CALIBRATION_PATTERN'] = data['calibration_pattern'].upper()

data['svg_template'] %= vars(args)
data['svg_output']   %= vars(args)
data['xml_template'] %= vars(args)
data['xml_output']   %= vars(args)

if data['svg_width'] is 0:
    if data['calibration_pattern'] == 'chessboard':
        data['svg_width'] = data['margin'] + (data['width'] + 1) * data['square_size'] + data['margin']
    elif data['calibration_pattern'] == 'asymmetric_circles_grid':
        data['svg_width'] = data['margin'] + 2 * data['width'] * data['square_size'] + data['margin']
    else:
        data['svg_width'] = data['margin'] + data['width'] * data['square_size'] + data['margin']

if data['svg_height'] is 0:
    if data['calibration_pattern'] == 'chessboard':
        data['svg_height'] = data['margin'] + (data['height'] + 1) * data['square_size'] + data['margin']
    else:
        data['svg_height'] = data['margin'] + data['height'] * data['square_size'] + data['margin']

if data['calibration_pattern'] == 'chessboard':
    data['margin_x'] = (data['svg_width'] - (data['width'] + 1) * data['square_size']) / 2
elif data['calibration_pattern'] == 'asymmetric_circles_grid':
    data['margin_x'] = (data['svg_width'] - 2 * data['width'] * data['square_size']) / 2
else:
    data['margin_x'] = (data['svg_width'] - data['width'] * data['square_size']) / 2

if data['calibration_pattern'] == 'chessboard':
    data['margin_y'] = (data['svg_height'] - (data['height'] + 1) * data['square_size']) / 2
else:
    data['margin_y'] = (data['svg_height'] - data['height'] * data['square_size']) / 2


with open(args.svg_template, 'r') as fp_in, open(args.svg_output, 'w') as fp_out:
    template = MarkupTemplate(fp_in.read())
    stream = template.generate(**data)
    fp_out.write(stream.render('xml'))

with open(args.xml_template, 'r') as fp_in, open(args.xml_output, 'w') as fp_out:
    template = MarkupTemplate(fp_in.read())
    stream = template.generate(**data)
    fp_out.write(stream.render('xml'))

